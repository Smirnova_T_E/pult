/********************************************************************************
** Form generated from reading UI file 'widget.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_WIDGET_H
#define UI_WIDGET_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Widget
{
public:
    QVBoxLayout *verticalLayout;
    QTabWidget *tabWidget;
    QWidget *tab;
    QVBoxLayout *verticalLayout_7;
    QHBoxLayout *horizontalLayout_2;
    QFrame *frame;
    QGroupBox *groupBox_5;
    QVBoxLayout *verticalLayout_6;
    QPushButton *ruchnoiBtn;
    QPushButton *autoBtn;
    QSpacerItem *verticalSpacer_3;
    QGroupBox *groupBox;
    QVBoxLayout *verticalLayout_2;
    QFormLayout *formLayout_2;
    QLabel *label_7;
    QLabel *desPsiLbl;
    QLabel *label_8;
    QLabel *curPsiLbl;
    QHBoxLayout *horizontalLayout;
    QPushButton *decPsiBtn;
    QPushButton *addPsiBtn;
    QSpacerItem *verticalSpacer_2;
    QGroupBox *groupBox_2;
    QVBoxLayout *verticalLayout_3;
    QFormLayout *formLayout_3;
    QLabel *label_11;
    QLabel *label_12;
    QLabel *label_13;
    QLabel *label_14;
    QSpinBox *spinBox;
    QSpacerItem *verticalSpacer;
    QPushButton *pushButton;
    QSpacerItem *horizontalSpacer;
    QWidget *tab_2;
    QLabel *label_23;
    QWidget *layoutWidget;
    QFormLayout *formLayout_6;
    QLabel *label_24;
    QLabel *label_27;
    QLabel *label_25;
    QLabel *label_28;
    QLabel *label_26;
    QLabel *label_29;

    void setupUi(QWidget *Widget)
    {
        if (Widget->objectName().isEmpty())
            Widget->setObjectName(QStringLiteral("Widget"));
        Widget->resize(618, 294);
        QFont font;
        font.setPointSize(12);
        Widget->setFont(font);
        verticalLayout = new QVBoxLayout(Widget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        tabWidget = new QTabWidget(Widget);
        tabWidget->setObjectName(QStringLiteral("tabWidget"));
        tab = new QWidget();
        tab->setObjectName(QStringLiteral("tab"));
        verticalLayout_7 = new QVBoxLayout(tab);
        verticalLayout_7->setSpacing(6);
        verticalLayout_7->setContentsMargins(11, 11, 11, 11);
        verticalLayout_7->setObjectName(QStringLiteral("verticalLayout_7"));
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        frame = new QFrame(tab);
        frame->setObjectName(QStringLiteral("frame"));
        QSizePolicy sizePolicy(QSizePolicy::Minimum, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(frame->sizePolicy().hasHeightForWidth());
        frame->setSizePolicy(sizePolicy);
        frame->setMinimumSize(QSize(150, 0));
        groupBox_5 = new QGroupBox(frame);
        groupBox_5->setObjectName(QStringLiteral("groupBox_5"));
        groupBox_5->setGeometry(QRect(9, -1, 150, 211));
        sizePolicy.setHeightForWidth(groupBox_5->sizePolicy().hasHeightForWidth());
        groupBox_5->setSizePolicy(sizePolicy);
        groupBox_5->setMinimumSize(QSize(150, 0));
        verticalLayout_6 = new QVBoxLayout(groupBox_5);
        verticalLayout_6->setSpacing(6);
        verticalLayout_6->setContentsMargins(11, 11, 11, 11);
        verticalLayout_6->setObjectName(QStringLiteral("verticalLayout_6"));
        ruchnoiBtn = new QPushButton(groupBox_5);
        ruchnoiBtn->setObjectName(QStringLiteral("ruchnoiBtn"));

        verticalLayout_6->addWidget(ruchnoiBtn);

        autoBtn = new QPushButton(groupBox_5);
        autoBtn->setObjectName(QStringLiteral("autoBtn"));

        verticalLayout_6->addWidget(autoBtn);


        horizontalLayout_2->addWidget(frame);

        verticalSpacer_3 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        horizontalLayout_2->addItem(verticalSpacer_3);

        groupBox = new QGroupBox(tab);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        QSizePolicy sizePolicy1(QSizePolicy::Expanding, QSizePolicy::Preferred);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(groupBox->sizePolicy().hasHeightForWidth());
        groupBox->setSizePolicy(sizePolicy1);
        verticalLayout_2 = new QVBoxLayout(groupBox);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        formLayout_2 = new QFormLayout();
        formLayout_2->setSpacing(6);
        formLayout_2->setObjectName(QStringLiteral("formLayout_2"));
        label_7 = new QLabel(groupBox);
        label_7->setObjectName(QStringLiteral("label_7"));

        formLayout_2->setWidget(0, QFormLayout::LabelRole, label_7);

        desPsiLbl = new QLabel(groupBox);
        desPsiLbl->setObjectName(QStringLiteral("desPsiLbl"));

        formLayout_2->setWidget(0, QFormLayout::FieldRole, desPsiLbl);

        label_8 = new QLabel(groupBox);
        label_8->setObjectName(QStringLiteral("label_8"));

        formLayout_2->setWidget(1, QFormLayout::LabelRole, label_8);

        curPsiLbl = new QLabel(groupBox);
        curPsiLbl->setObjectName(QStringLiteral("curPsiLbl"));

        formLayout_2->setWidget(1, QFormLayout::FieldRole, curPsiLbl);


        verticalLayout_2->addLayout(formLayout_2);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        decPsiBtn = new QPushButton(groupBox);
        decPsiBtn->setObjectName(QStringLiteral("decPsiBtn"));

        horizontalLayout->addWidget(decPsiBtn);

        addPsiBtn = new QPushButton(groupBox);
        addPsiBtn->setObjectName(QStringLiteral("addPsiBtn"));

        horizontalLayout->addWidget(addPsiBtn);


        verticalLayout_2->addLayout(horizontalLayout);


        horizontalLayout_2->addWidget(groupBox);

        verticalSpacer_2 = new QSpacerItem(30, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        horizontalLayout_2->addItem(verticalSpacer_2);

        groupBox_2 = new QGroupBox(tab);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
        verticalLayout_3 = new QVBoxLayout(groupBox_2);
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setContentsMargins(11, 11, 11, 11);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        formLayout_3 = new QFormLayout();
        formLayout_3->setSpacing(6);
        formLayout_3->setObjectName(QStringLiteral("formLayout_3"));
        label_11 = new QLabel(groupBox_2);
        label_11->setObjectName(QStringLiteral("label_11"));

        formLayout_3->setWidget(0, QFormLayout::LabelRole, label_11);

        label_12 = new QLabel(groupBox_2);
        label_12->setObjectName(QStringLiteral("label_12"));

        formLayout_3->setWidget(0, QFormLayout::FieldRole, label_12);

        label_13 = new QLabel(groupBox_2);
        label_13->setObjectName(QStringLiteral("label_13"));

        formLayout_3->setWidget(1, QFormLayout::LabelRole, label_13);

        label_14 = new QLabel(groupBox_2);
        label_14->setObjectName(QStringLiteral("label_14"));

        formLayout_3->setWidget(1, QFormLayout::FieldRole, label_14);

        spinBox = new QSpinBox(groupBox_2);
        spinBox->setObjectName(QStringLiteral("spinBox"));
        spinBox->setMinimum(-10);
        spinBox->setMaximum(100);

        formLayout_3->setWidget(3, QFormLayout::SpanningRole, spinBox);

        verticalSpacer = new QSpacerItem(20, 38, QSizePolicy::Minimum, QSizePolicy::Expanding);

        formLayout_3->setItem(2, QFormLayout::SpanningRole, verticalSpacer);

        pushButton = new QPushButton(groupBox_2);
        pushButton->setObjectName(QStringLiteral("pushButton"));

        formLayout_3->setWidget(5, QFormLayout::SpanningRole, pushButton);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        formLayout_3->setItem(4, QFormLayout::SpanningRole, horizontalSpacer);


        verticalLayout_3->addLayout(formLayout_3);


        horizontalLayout_2->addWidget(groupBox_2);


        verticalLayout_7->addLayout(horizontalLayout_2);

        tabWidget->addTab(tab, QString());
        tab_2 = new QWidget();
        tab_2->setObjectName(QStringLiteral("tab_2"));
        label_23 = new QLabel(tab_2);
        label_23->setObjectName(QStringLiteral("label_23"));
        label_23->setGeometry(QRect(20, 10, 181, 31));
        layoutWidget = new QWidget(tab_2);
        layoutWidget->setObjectName(QStringLiteral("layoutWidget"));
        layoutWidget->setGeometry(QRect(20, 70, 193, 88));
        formLayout_6 = new QFormLayout(layoutWidget);
        formLayout_6->setSpacing(6);
        formLayout_6->setContentsMargins(11, 11, 11, 11);
        formLayout_6->setObjectName(QStringLiteral("formLayout_6"));
        formLayout_6->setContentsMargins(0, 0, 0, 0);
        label_24 = new QLabel(layoutWidget);
        label_24->setObjectName(QStringLiteral("label_24"));

        formLayout_6->setWidget(0, QFormLayout::LabelRole, label_24);

        label_27 = new QLabel(layoutWidget);
        label_27->setObjectName(QStringLiteral("label_27"));

        formLayout_6->setWidget(0, QFormLayout::FieldRole, label_27);

        label_25 = new QLabel(layoutWidget);
        label_25->setObjectName(QStringLiteral("label_25"));

        formLayout_6->setWidget(1, QFormLayout::LabelRole, label_25);

        label_28 = new QLabel(layoutWidget);
        label_28->setObjectName(QStringLiteral("label_28"));

        formLayout_6->setWidget(1, QFormLayout::FieldRole, label_28);

        label_26 = new QLabel(layoutWidget);
        label_26->setObjectName(QStringLiteral("label_26"));

        formLayout_6->setWidget(2, QFormLayout::LabelRole, label_26);

        label_29 = new QLabel(layoutWidget);
        label_29->setObjectName(QStringLiteral("label_29"));

        formLayout_6->setWidget(2, QFormLayout::FieldRole, label_29);

        tabWidget->addTab(tab_2, QString());

        verticalLayout->addWidget(tabWidget);


        retranslateUi(Widget);

        tabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(Widget);
    } // setupUi

    void retranslateUi(QWidget *Widget)
    {
        Widget->setWindowTitle(QApplication::translate("Widget", "Widget", 0));
        groupBox_5->setTitle(QApplication::translate("Widget", "\320\240\320\265\320\266\320\270\320\274", 0));
        ruchnoiBtn->setText(QApplication::translate("Widget", "\320\240\321\203\321\207\320\275\320\276\320\271", 0));
        autoBtn->setText(QApplication::translate("Widget", "\320\220\320\262\321\202\320\276", 0));
        groupBox->setTitle(QApplication::translate("Widget", "\320\232\321\203\321\200\321\201", 0));
        label_7->setText(QApplication::translate("Widget", "\320\227\320\260\320\264\320\260\320\275\320\275\321\213\320\271", 0));
        desPsiLbl->setText(QApplication::translate("Widget", "TextLabel", 0));
        label_8->setText(QApplication::translate("Widget", "\320\242\320\265\320\272\321\203\321\211\320\270\320\271", 0));
        curPsiLbl->setText(QApplication::translate("Widget", "TextLabel", 0));
        decPsiBtn->setText(QApplication::translate("Widget", "-", 0));
        addPsiBtn->setText(QApplication::translate("Widget", "+", 0));
        groupBox_2->setTitle(QApplication::translate("Widget", "\320\236\321\202\321\201\321\202\320\276\321\217\320\275\320\270\320\265 \320\276\321\202 \320\264\320\275\320\260", 0));
        label_11->setText(QApplication::translate("Widget", "\320\227\320\260\320\264\320\260\320\275\320\275\320\276\320\265", 0));
        label_12->setText(QApplication::translate("Widget", "TextLabel", 0));
        label_13->setText(QApplication::translate("Widget", "\320\242\320\265\320\272\321\203\321\211\320\265\320\265", 0));
        label_14->setText(QApplication::translate("Widget", "TextLabel", 0));
        pushButton->setText(QApplication::translate("Widget", "\320\236\320\272", 0));
        tabWidget->setTabText(tabWidget->indexOf(tab), QApplication::translate("Widget", "\320\243\320\277\321\200\320\260\320\262\320\273\320\265\320\275\320\270\320\265", 0));
        label_23->setText(QApplication::translate("Widget", "\320\236\320\261\320\274\320\265\320\275 \321\201 \320\235\320\237\320\220", 0));
        label_24->setText(QApplication::translate("Widget", "ip", 0));
        label_27->setText(QApplication::translate("Widget", "TextLabel", 0));
        label_25->setText(QApplication::translate("Widget", "port", 0));
        label_28->setText(QApplication::translate("Widget", "TextLabel", 0));
        label_26->setText(QApplication::translate("Widget", "\320\241\320\276\321\201\321\202\320\276\321\217\320\275\320\270\320\265", 0));
        label_29->setText(QApplication::translate("Widget", "TextLabel", 0));
        tabWidget->setTabText(tabWidget->indexOf(tab_2), QApplication::translate("Widget", "\320\241\321\202\320\260\321\202\320\270\321\201\321\202\320\270\320\272\320\260", 0));
    } // retranslateUi

};

namespace Ui {
    class Widget: public Ui_Widget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_WIDGET_H
