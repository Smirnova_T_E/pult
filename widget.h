#ifndef WIDGET_H
#define WIDGET_H

#include "ui_widget.h"
#include "pult_protocol.h"
#include <QTimer>

class Widget : public QWidget, private Ui::Widget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    PultProtocol *protocol;
    QTimer timer;
public slots:
    void updateValues();
    void height_update();
private slots:
    void on_addPsiBtn_clicked();
    void on_decPsiBtn_clicked();
    void on_ruchnoiBtn_clicked();
    void on_autoBtn_clicked();
};

#endif // WIDGET_H
